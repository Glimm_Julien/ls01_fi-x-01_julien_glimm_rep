import java.util.Scanner;
import java.util.Arrays;

public class Aufgaben {

	public static void main(String[] args) {

		Aufgabe1();
		Aufgabe2();
	}

	public static int Aufgabe1() {
		Scanner scr = new Scanner(System.in);

		int[] array = new int[5];

		for (int i = 0; i < array.length; i++) {
			array[i] = scr.nextInt();

		}
		for (int j = 4; j >= 0; j--) {
			System.out.println(array[j]);

		}
		return 0;
	}

	public static void Aufgabe2() {

		int[] array1 = { 3, 7, 12, 18, 37, 42 };

		for (int i = 0; i <= 5; i++) {
			System.out.printf("% d", array1[i]);

		}
		for (int i = 0; i <= 5; i++) {

			if (array1[i] == 12 && array1[i] != 13) {

				System.out.println("");
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
				System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
			}

		}
	}
}
