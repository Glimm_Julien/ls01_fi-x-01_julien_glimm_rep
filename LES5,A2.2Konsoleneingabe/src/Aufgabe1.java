import java.util.Scanner; // Import der Klasse Scanner
public class Aufgabe1    
{
 
 public static void main(String[] args) // Hier startet das Programm
 {
 
 // Neues Scanner-Objekt myScanner wird erstellt 
 Scanner myScanner = new Scanner(System.in); 
 
 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
 
 // Die Variable zahl1 speichert die erste Eingabe
 float zahl1 = myScanner.nextFloat();
 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
 
 // Die Variable zahl2 speichert die zweite Eingabe
 float zahl2 = myScanner.nextFloat(); 
 // Die Addition der Variablen zahl1 und zahl2 
 // wird der Variable ergebnis zugewiesen.
 float ergebnis = zahl1 + zahl2;
 
 float ergebnis2 = zahl1 - zahl2;
 
 float ergebnis3 = zahl1 / zahl2;
 
 float ergebnis4 = zahl1 * zahl2;
 
 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis); 
 myScanner.close();
 
 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis2); 
 myScanner.close();
 
 System.out.print("\n\n\nErgebnis der Division lautet: ");
 System.out.print(zahl1 + " : " + zahl2 + " = " + ergebnis3); 
 myScanner.close();
 
 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis4); 
 myScanner.close();
 
 } 
} 
