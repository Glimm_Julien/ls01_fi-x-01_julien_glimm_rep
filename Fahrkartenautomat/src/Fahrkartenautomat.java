﻿import java.util.Scanner;
import java.util.ArrayList;

public class Fahrkartenautomat {

	public static void main(String[] args) {
		while (true) {

			int ihreWahl;
			System.out.println("");

			double zuZahlenderBetrag = 0.0;

			do {
				ihreWahl = fahrkartenbestellungErfassung();

				if (ihreWahl > 10 || ihreWahl < 0) {

					System.out.println("Ihre Wahl " + ihreWahl);
					System.out.println(">>falsche Eingabe<<");
				} else if (ihreWahl != 0) {
					zuZahlenderBetrag = zuZahlenderBetrag + fahrkartenbestellungErfassen(ihreWahl);
				}

			} while (ihreWahl != 0);

			double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, die Fahrscheine\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");

		}
	}

	public static int fahrkartenbestellungErfassung() {
		Scanner tastatur = new Scanner(System.in);
		int ihreWahl;
		String[] namen = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin ABC", "Einzelfahrschein Berlin ABC",
				"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC", "Bezahlen" };
		double[] preis = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		int[] nummer = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0 };
		System.out.println("Auswahlnummer		Bezeichnung					   Preis in Euro");
		for (int i = 0; i <= 9; i++) {

			System.out.printf("%-12s%34s%35s%n", nummer[i], namen[i], preis[i]);

		}

		System.out.println("Geben Sie das gewünschte Ticket ein: ");
		System.out.println("Drücken sie [0] um zu bezahlen");

		ihreWahl = tastatur.nextInt();

		return ihreWahl;
	}

	public static double fahrkartenbestellungErfassen(int fahrkartenart) {
		Scanner tastatur = new Scanner(System.in);

		double anzahlTickets;

		double ticketEinzelpreis = 0;

		if (fahrkartenart == 1) {
			System.out.println("Preis pro Tickes: 2,90€ ");
			ticketEinzelpreis = 2.90;
		} else if (fahrkartenart == 2) {
			System.out.println("Preis pro Ticket: 3,30€ ");
			ticketEinzelpreis = 3.30;
		} else if (fahrkartenart == 3) {
			System.out.println("Preis pro Ticket: 3,60€ ");
			ticketEinzelpreis = 3.60;
		} else if (fahrkartenart == 4) {
			System.out.println("Preis pro Ticket: 1,90€ ");
			ticketEinzelpreis = 1.90;
		} else if (fahrkartenart == 5) {
			System.out.println("Preis pro Ticket: 8,60€ ");
			ticketEinzelpreis = 8.60;
		} else if (fahrkartenart == 6) {
			System.out.println("Preis pro Ticket: 9,00€ ");
			ticketEinzelpreis = 9.00;
		} else if (fahrkartenart == 7) {
			System.out.println("Preis pro Ticket: 9,60€ ");
			ticketEinzelpreis = 9.60;
		} else if (fahrkartenart == 8) {
			System.out.println("Preis pro Ticket: 23,50€ ");
			ticketEinzelpreis = 23.50;
		} else if (fahrkartenart == 9) {
			System.out.println("Preis pro Ticket: 24,30€ ");
			ticketEinzelpreis = 24.30;
		} else if (fahrkartenart == 10) {
			System.out.println("Preis pro Ticket: 24,90€ ");
			ticketEinzelpreis = 24.90;
		}

		do {
			System.out.print("Wie viele Tickets werden gekauft?(Max. 10): ");
			anzahlTickets = tastatur.nextInt();

			if (anzahlTickets > 10 || anzahlTickets < 0) {

				System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}

		} while (anzahlTickets > 10 || anzahlTickets < 0);

		return ticketEinzelpreis * anzahlTickets;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

		{
			System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
					"  ");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden ausgegeben.");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.println("%s%.2f Der Rückgabebetrag in Höhe von %s " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("   * * *");
				System.out.println(" * 	  *");
				System.out.println("*    2     *");
				System.out.println("*   EURO   *");
				System.out.println(" *        *");
				System.out.println("   * * *");
				System.out.println("");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{	System.out.println("   * * *");
				System.out.println(" * 	  *");
				System.out.println("*    1     *");
				System.out.println("*   EURO   *");
				System.out.println(" *        *");
				System.out.println("   * * *");
				System.out.println("");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("   * * *");
				System.out.println(" * 	  *");
				System.out.println("*    50    *");
				System.out.println("*   CENT   *");
				System.out.println(" *        *");
				System.out.println("   * * *");
				System.out.println("");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("   * * *");
				System.out.println(" * 	  *");
				System.out.println("*    20     *");
				System.out.println("*   CENT   *");
				System.out.println(" *        *");
				System.out.println("   * * *");
				System.out.println("");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("   * * *");
				System.out.println(" * 	  *");
				System.out.println("*    10     *");
				System.out.println("*   CENT   *");
				System.out.println(" *        *");
				System.out.println("   * * *");
				System.out.println("");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("   * * *");
				System.out.println(" * 	  *");
				System.out.println("*    5     *");
				System.out.println("*   CENT   *");
				System.out.println(" *        *");
				System.out.println("   * * *");
				System.out.println("");
				rückgabebetrag -= 0.05;
			}
		}

	}
}